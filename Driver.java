import java.util.Scanner;

public class Driver {
	
	public static void main (String[] args) {
	
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Hello");
		System.out.println("Add num 1");
		
		int x = reader.nextInt();
		
		System.out.println("Add num 2");
		int y = reader.nextInt();
		
		System.out.println("Num1 + Num 2 = " + Calculator.addNum(x, y));
		
		System.out.println("Add num for square root");
		
		int z = reader.nextInt();
		
		System.out.println("Square root of num is: " + Calculator.squareRoot(z));
		
		System.out.println("Here is a random number: " + Calculator.randomNumber());
	}
	
}