import java.lang.Math;
import java.util.Random;

public class Calculator {
	
	public static int addNum(int x, int y) {
		return x + y;
	}

	public static double squareRoot(int z) {
		double answer = Math.sqrt(z);
		return answer;
	}
	
	public static double randomNumber() {
		Random rand = new Random();
		return Rand.nextInt();
	}
	
	public static double divideNumber(int x, int y) {
		
		if (x == 0 || y == 0) {
			System.out.println("Not possible");
		}
		
		return x / y;
	}
}